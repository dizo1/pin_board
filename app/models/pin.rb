class Pin < ApplicationRecord
  belongs_to :user
  acts_as_votable

  validates :title, presence: true
  validates :description, presence: true
  has_attached_file :image, style: { :medium => "300x300>" }
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
