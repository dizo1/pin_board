ActiveAdmin.register Pin do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
 permit_params :title, :description
#
# or

index do
  column :user_id
  column :title
  column :description
  actions
end
end
