class ApplicationMailer < ActionMailer::Base
  default from: 'mugandadizo@gmail.com'
  layout 'mailer'
end
