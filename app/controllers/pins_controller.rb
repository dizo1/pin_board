class PinsController < ApplicationController
  before_action :set_pin, only: [:edit, :show, :update, :destroy, :upvote ]
  before_action :authenticate_user!, :except => [:index, :show]
  def index
    @pins = Pin.all.order("created_at DESC")
  end

  def show

  end

  def new
    @pin = current_user.pins.build
  end

  def create
    @pin = current_user.pins.build(pin_params)
    if @pin.save
      redirect_to @pin, notice: "Pin successfully created"
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @pin.update(pin_params)
      redirect_to @pin, flash[:notice] = "Update was successful"
    else
      render 'edit'
    end
  end

  def destroy
    @pin.destroy
    redirect_to root_path
  end

  def upvote
    @pin.upvote_by current_user
    redirect_to pin_path(@pin)
  end

  private

  def pin_params
    params.require(:pin).permit(:title, :description, :image)
  end
  def set_pin
    @pin = Pin.find(params[:id])
  end
end
